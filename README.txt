*********************************************************
Detecting Most Popular Topics from Live Twitter
Message Streams using the
Lossy Counting Algorithm with Apache Storm
********************************************************

This project contains everything required to implement
a real-time streaming data analytics system using
Apache Storm, detecting the most frequent occurring
hashtags from the live Twitter data stream in real-time.

*******************************************************
To compile:
>mvn package

To run the topology locally:
>storm jar target/LossyTwitter-0.1.0.jar TTTopology

To run the topology remotely:
>storm jar target/LossyTwitter-0.1.0.jar TTTopology remote

To run the topology in parallel:
>storm jar target/LossyTwitter-0.1.0.jar TTTopology TwitTop4 remote pset

Optional arguments:
pset       Set this if you want to use the configured parallelization
            (nExecs=2, nTasks=4, nWorkers=4)
sBuckets=  Set for customized number of buckets (default=200)
sThresh=   Set for customized threshold (default none)
nExecs=    Set for customized number of executors
nTasks=    Set for customized number of tasks
nWorkers=  Set for customized number of workers
oFreq=      Set for customized output frequency (s)

*******************************************************
To open the Storm UI:
>xdg-open http://juneau.cs.colostate.edu:31499

******************************************************
Included Information:
In the info directory:
-- Topology Figure (Twitter_Topology.png)

-- Screen shot of non-parallel setup (nParallel.png)
-- Sample output for non-parallel setup (TwitterReport1.txt)

-- Screen shot of parallel setup (Parallel.png)
-- Sample output for parallel setup (TwitterReport4.txt)

-- Testing Topology with the simple number example stream.
   To run:
   >storm jar target/LossyTwitter-0.1.0.jar TestTopology
       (TestTop remote added for parallel)

**********************************************************
Katherine Haynes, CS535
March 10, 2020