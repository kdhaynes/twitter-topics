import org.apache.storm.Config;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.base.BaseWindowedBolt;
import org.apache.storm.topology.base.BaseWindowedBolt.Count;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.utils.TupleUtils;
import org.apache.storm.windowing.TupleWindow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.LinkedList;


public class ReportBolt extends BaseWindowedBolt {

   private String dirOut = "/s/chopin/a/grad/kdcorbin/storm-2.1.0/examples/LossyTwitter/info/";
   private String fileName = "ReportBoltOut.txt";
   private static final int TOP_N = 100;

   private OutputCollector collector;
   private static final Logger LOG = LoggerFactory.getLogger(ReportBolt.class);

   private Path filePath;
   private Timestamp startTimestamp;
   private int topN;
   private String fileOut;

   private Map<String, Integer> reports = new HashMap<String, Integer>();

   public ReportBolt() {
         topN = TOP_N;
	 fileOut = dirOut + fileName;
   }
   
   public ReportBolt(int topN) {
         this.topN = topN;
	 fileOut = dirOut + fileName;
   }

   public ReportBolt(String fName) {
         this.topN = TOP_N;
	 fileOut = dirOut + fName;
   }

   public ReportBolt(int topN, String fName) {
         this.topN = topN;
	 this.fileOut = dirOut + fName;
   }

   @Override
   public void prepare(Map conf, TopologyContext context, OutputCollector collector) {
       this.collector = collector;
       startTimestamp = new Timestamp(System.currentTimeMillis());

       filePath = Paths.get(fileOut);
       try {
            LOG.info("Writing Initial Timestamp To File: ", fileOut);
            String greeting = "Log Date: " + startTimestamp.toString() + System.lineSeparator();
            Files.write(filePath, greeting.getBytes());
       } catch (Exception error) {
            LOG.info("Error Writing To File: " + fileOut);
	    LOG.info("Error Message: " + error);
       }
   }

   //Execute to write output
   @Override
   public void execute(TupleWindow inputWindow) {

          //Setup Info
	  List<Tuple> tuples = inputWindow.get();
          LOG.info("TICKTUPLE: Time To Write Output");
	  //LOG.info("Events in current window: " + tuples.size());
          //LOG.info("TUPLES: " + tuples);

          Timestamp timestamp = new Timestamp(System.currentTimeMillis());
          Long timeNow = timestamp.getTime();
          String fileLine = timeNow.toString() + " ";
	  int lenOnlyTime = fileLine.length();

          //Sort
          //Map<String, Integer> reports = new HashMap<String, Integer>();
          for (Tuple t : tuples) {
	      String key = t.getStringByField("key");
	      int val = t.getIntegerByField("count");
	      if (val <= 0) {
                 reports.remove(key);
              } else {
                 reports.put(key, val);
		 //LOG.info("ADDED: " + key + " " + val);
              }
              collector.ack(t);
	  }

         // Create a list from elements of HashMap 
         List<Map.Entry<String, Integer> > list = 
               new LinkedList<Map.Entry<String, Integer> >(reports.entrySet()); 

         // Sort the list 
         Collections.sort(list, new Comparator<Map.Entry<String, Integer> >() { 
            public int compare(Map.Entry<String, Integer> o1,  
                               Map.Entry<String, Integer> o2) 
            {
	        int compval = (o2.getValue()).compareTo(o1.getValue());
		if (compval == 0) {
		    return -1;
	        } else {
		    return compval;
	        }
	    }
          }); 

          int index = 0;
          for(Map.Entry<String,Integer> t : list) {
	      if (t.getValue() > 0) {
                  fileLine += t.getKey() + ":" + t.getValue() + " ";
	          index++;
              }

              if (index >= topN){
	          break;
              }
	  }

          //Write out the line
	  if (fileLine.length() > lenOnlyTime) {
	      fileLine += System.lineSeparator();
              try {
                  Files.write(filePath, fileLine.getBytes(), StandardOpenOption.APPEND);
                  //LOG.info("Wrote Updated Hashtags.");
              } catch (Exception error) {
                  LOG.info("Writing Exception: " + error);
              }
	   }
   }

   @Override
   public void declareOutputFields(OutputFieldsDeclarer declarer) {}

   @Override
   public void cleanup() {}

   
}
       