import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import org.apache.storm.utils.Utils;

import java.util.Map;

public class TestSpout extends BaseRichSpout {
    private SpoutOutputCollector collector;
    private String[] nums = {
       "1", "2", "4", "3", "4",
       "3", "4", "5", "4", "6",
       "7", "3", "3", "6", "1",
       "1", "3", "2", "4", "7"
     };

     private int index = 0;
     public void declareOutputFields(OutputFieldsDeclarer declarer) {
           declarer.declare(new Fields("hashtag"));
     }

     public void open(Map config, TopologyContext context, SpoutOutputCollector collector){
         this.collector = collector;
     }

     public void nextTuple() {
         this.collector.emit(new Values(nums[index]));
	 index ++;
	 if (index >= nums.length){
	     index = 0;
	 }

         Utils.sleep(1);

     }

}