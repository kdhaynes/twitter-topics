/**
 * Compile:
 * >mvn package
 *
 * Run:
 * >storm jar target/LossyTwitter-0.1.0.jar TestTopology
 *
 * >storm jar target/LossyTwitter-0.1.0.jar TestTopology TestTop remote
**/


import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.StormSubmitter;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.windowing.TupleWindow;
import org.apache.storm.topology.base.BaseWindowedBolt;
import org.apache.storm.tuple.Fields;

import java.util.concurrent.TimeUnit;
import static org.apache.storm.topology.base.BaseWindowedBolt.Duration;
import static org.apache.storm.topology.base.BaseWindowedBolt.Count;

public class TestTopology {

   private static int BUCKET_SIZE = 5;
   private static int EXECS_NUM = 2;
   private static int TASKS_NUM = 4;
   private static int WORKERS_NUM = 4;
   private static double STHRESH = 0.8;

   //Entry point for topology
   public static void main(String[] args) throws Exception {

       TopologyBuilder builder = new TopologyBuilder();

       //Test Spout
       builder.setSpout("test-spout", new TestSpout(), 1);

       //Lossy Counting Test Bolt
       BaseWindowedBolt lcbolt = new LossyCountBolt(BUCKET_SIZE, STHRESH)
              .withTumblingWindow(BaseWindowedBolt.Count.of(BUCKET_SIZE));
       builder.setBolt("lossycount-bolt", lcbolt, EXECS_NUM)
              .setNumTasks(TASKS_NUM)
              .fieldsGrouping("test-spout", new Fields("hashtag"));

       //Report Test Bolt
       BaseWindowedBolt rbolt = new ReportBolt(100)
              .withTumblingWindow(BaseWindowedBolt.Duration.seconds(4));
       builder.setBolt("report-bolt", rbolt, 1)
	      .globalGrouping("lossycount-bolt");

       //Configuration
       Config conf = new Config();
       conf.setNumWorkers(WORKERS_NUM);
       conf.setDebug(true);

       //Run on cluster if there are arguments
       if (args != null && args.length > 0) {
	   //submit the topology
	   StormSubmitter.submitTopology(args[0], conf, builder.createTopology());
       }
       //Otherwise run locally
       else {
           //Cap maximum number of executors that can be spawned
	   conf.setMaxTaskParallelism(1);

           //LocalCluster is used to run locally
	   LocalCluster cluster = new LocalCluster();

           //Submit the topology
	   cluster.submitTopology("TwitterTop", conf, builder.createTopology());

           //Sleep
	   Thread.sleep(10000);

           //Stop topology
	   cluster.killTopology("TwitterTop");
	   
           //Shut down cluster
	   cluster.shutdown();
       }

   }
}