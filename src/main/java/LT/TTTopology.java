/**
 * Compile:
 * >mvn package
 *
 * Run:
 * >storm jar target/LossyTwitter-0.1.0.jar TTTopology
 *
 * >storm jar target/LossyTwitter-0.1.0.jar TTTopology TwitterTop remote
**/


import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.StormSubmitter;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.windowing.TupleWindow;
import org.apache.storm.topology.base.BaseWindowedBolt;
import org.apache.storm.tuple.Fields;

import java.util.concurrent.TimeUnit;
import static org.apache.storm.topology.base.BaseWindowedBolt.Duration;
import static org.apache.storm.topology.base.BaseWindowedBolt.Count;

public class TTTopology {

   //For parallel:
   private static int EXECS_NUM = 2;
   private static int TASKS_NUM = 4;
   private static int WORKERS_NUM = 4;

   //For non-parallel:
   private static int EXECS_NUM1 = 1;
   private static int TASKS_NUM1 = 1;
   private static int WORKERS_NUM1 = 1;

   //User-Defined Parameters:
   private static int BUCKET_SIZE = 200;
   private static double S_THRESH = -1;
   private static int OUTPUT_DUR = 10;
   private static int TOP_N = 100;
   
   //Entry point for topology
   public static void main(String[] args) throws Exception {

       //Selected Parameters:
       boolean inParallel = false;
       int nBuckets = BUCKET_SIZE;
       int nExecs = EXECS_NUM1;
       int nTasks = TASKS_NUM1;
       int nWorkers = WORKERS_NUM1;
       int oFreq = OUTPUT_DUR;
       double sThresh = S_THRESH;
       String nameTopology = "TwitterTop";

       // Set variables depending on run type
       // Parallel or Non-Parallel
       if (args!= null && args.length > 0) {
           for (String s: args) {
                int bucketIndx = s.indexOf("sBuckets=");
		int threshIndx = s.indexOf("sThresh=");
		int remoteIndx = s.indexOf("remote");
		int psetIndx = s.indexOf("pset");
		int nexIndx = s.indexOf("nExecs=");
		int ntIndx = s.indexOf("nTasks=");
		int nwIndx = s.indexOf("nWorkers=");
		int odIndx = s.indexOf("oDur=");
		
		if (bucketIndx >= 0) {
		   nBuckets = Integer.parseInt(s.substring(bucketIndx+9,s.length()));
		} else if (threshIndx >= 0) {
		   sThresh = Double.parseDouble(s.substring(threshIndx+8,s.length()));
		} else if (remoteIndx >= 0) {
		   inParallel = true;
		} else if (psetIndx >= 0) {
		   nExecs = EXECS_NUM;
		   nTasks = TASKS_NUM;
		   nWorkers = WORKERS_NUM;
		} else if (nexIndx >= 0) {
		   nExecs = Integer.parseInt(s.substring(nexIndx+7,s.length()));
		} else if (ntIndx >= 0) {
		   nTasks = Integer.parseInt(s.substring(ntIndx+7,s.length()));
		 } else if (nwIndx >= 0) {
		   nWorkers = Integer.parseInt(s.substring(nwIndx+9,s.length()));
		} else if (odIndx >= 0) {
                   oFreq = Integer.parseInt(s.substring(odIndx+5,s.length()));
                } else {
		   nameTopology = s;
		}
           }	   
       } 

       System.out.println("Topology Name: " + nameTopology);
       System.out.println("Bucket Size: " + nBuckets);
       System.out.println("Threshold: " + sThresh);
       System.out.println("Parallel: " + inParallel);
       System.out.println("nExecs: " + nExecs);
       System.out.println("nTasks: " + nTasks);
       System.out.println("nWorkers: " + nWorkers);
       System.out.println("Output Frequency: " + oFreq);

       TopologyBuilder builder = new TopologyBuilder();

       //Twitter Spout
       builder.setSpout("twitter-spout", new TwitterSpout(), 1);

       //Hashtag Bolt
       builder.setBolt("hashtag-bolt", new HashtagBolt(), nExecs)
              .setNumTasks(nTasks)
              .shuffleGrouping("twitter-spout");

       //Lossy Counting Algorithm Bolt
       BaseWindowedBolt lcbolt = new LossyCountBolt(nBuckets, sThresh)
               .withTumblingWindow(BaseWindowedBolt.Count.of(nBuckets));
       builder.setBolt("lossycount-bolt", lcbolt, nExecs)
              .setNumTasks(nTasks)
              .fieldsGrouping("hashtag-bolt", new Fields("hashtag"));

       //Report Bolt
       String fileName = "TwitterReport" + nTasks + ".txt";
       BaseWindowedBolt rbolt = new ReportBolt(TOP_N, fileName)
           .withTumblingWindow(BaseWindowedBolt.Duration.seconds(oFreq));
       builder.setBolt("report-bolt", rbolt, 1)
           .globalGrouping("lossycount-bolt");

       //Configuration
       Config conf = new Config();
       conf.setNumWorkers(nWorkers);

       //Run on cluster if there are arguments
       if (inParallel) {	   
	   //submit the topology
	   StormSubmitter.submitTopology(nameTopology, conf, builder.createTopology());
       }
       //Otherwise run locally
       else {
           //Cap maximum number of executors that can be spawned
	   conf.setMaxTaskParallelism(1);

           //LocalCluster is used to run locally
	   LocalCluster cluster = new LocalCluster();

           //Submit the topology
	   cluster.submitTopology(nameTopology, conf, builder.createTopology());

           //Sleep
	   Thread.sleep(10000);

           //Stop topology
	   cluster.killTopology(nameTopology);
	   
           //Shut down cluster
	   cluster.shutdown();
       }

   }
}