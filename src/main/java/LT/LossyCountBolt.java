import org.apache.storm.Config;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseWindowedBolt;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import org.apache.storm.utils.TupleUtils;
import org.apache.storm.windowing.TupleWindow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

public class LossyCountBolt extends BaseWindowedBolt {

    private static final Logger LOG = LoggerFactory.getLogger(LossyCountBolt.class);

    private OutputCollector collector;

    private Map<String, int[]> counts = new HashMap<String, int[]>();

    private int currBucket, sizeBucket;
    private int lcN;
    private double epsilon, lcS;

    public LossyCountBolt(int sizeBucket) {
       this.sizeBucket =  sizeBucket;
       lcN = 0;
       lcS = -1.;
       currBucket = 1;
       epsilon = 1./((double) sizeBucket);
    }

    public LossyCountBolt(int sizeBucket, double sThresh) {
        this.sizeBucket = sizeBucket;
	this.lcS = sThresh;
	lcN = 0;
	currBucket = 1;
	epsilon = 1./((double) sizeBucket);
    }

    @Override
    public void prepare(Map conf, TopologyContext context, OutputCollector collector) {
       this.collector = collector;
    }

    @Override
    public void execute(TupleWindow tWindow) {
        List<Tuple> tuples = tWindow.get();
	//LOG.info("IN EXECUTE LossyCountBolt: " + tuples.size());
	for (Tuple t : tuples) {
	     String key = t.getStringByField("hashtag");
	     //LOG.info("HASHHERE: " + key);

             //Insert Phase
	     int[] vals = counts.get(key);
	     if (vals == null) {
	        vals = new int[]{1,currBucket-1};
	        counts.put(key, vals);
	     } else if (vals[0] == 0) {
	        vals[0] = 1;
		vals[1] = currBucket-1;
	     } else {
	        vals[0]++;
		counts.replace(key, vals);
	     }
            //LOG.info("LOGGED: " + key + " " + vals[0]);
	    
            lcN++;
            collector.ack(t);
        }

        //Delete Phase
	for(Map.Entry<String, int[]> mapElement : counts.entrySet()) {
	    String key = mapElement.getKey();
	    int[] vals = mapElement.getValue();
            if (vals[0]+vals[1] <= currBucket) {
	        vals[0] = 0;
		vals[1] = 0;
		counts.put(key,vals);
            }
	}

        currBucket++;

        for(Map.Entry<String, int[]> mapElement : counts.entrySet()) {
	  String key = mapElement.getKey();
	  int[] vals = mapElement.getValue();
	  double fThresh = 0.;
	  if (lcS > 0) {
               fThresh = (lcS - epsilon)*lcN;
	       LOG.info("SETTING THRESHOLD: " + fThresh);
	  }
	  if (vals[0] > fThresh) {
               collector.emit(new Values(key,vals[0]));
	  } else {
               collector.emit(new Values(key,0));
	  }
	}

    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
         declarer.declare(new Fields("key","count"));
    }

    @Override
    public void cleanup() {}

}