/** HashtagBolt processes the tweet and emits all the available hashtags
**/


import java.util.Map;
import java.util.StringTokenizer;
import twitter4j.*;
import twitter4j.conf.*;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import org.apache.commons.lang.StringUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HashtagBolt extends BaseRichBolt {

    private OutputCollector collector;
    private static final Logger LOG = LoggerFactory.getLogger(HashtagBolt.class);

    @Override
    public void prepare(Map conf, TopologyContext context, OutputCollector collector) {
         this.collector = collector;
    }

    @Override
    public void execute(Tuple tuple) {
        //LOG.info("IN EXECUTE HashtagBolt");
        String tweet = tuple.getStringByField("tweet");
	tweet = tweet.replaceAll(",", "\\s");
	//tweet = tweet.replaceAll(".", "\\s");
        StringTokenizer st = new StringTokenizer(tweet);
	while(st.hasMoreElements()) {
	    String tmp = (String) st.nextElement();
	    if(StringUtils.startsWith(tmp, "#")) {
	       //LOG.info("Hashtag: " + tmp);
	       collector.emit(new Values(tmp));
	    }
	}
	collector.ack(tuple);
   }

    @Override
    public void cleanup(){}

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
       declarer.declare(new Fields("hashtag"));
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
      return null;
    }

}