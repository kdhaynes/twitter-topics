
import org.apache.storm.Config;
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import org.apache.storm.utils.Utils;

import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

public class TwitterSpout extends BaseRichSpout {

    private String consumerKey = "mRt6sSNeFgwgTIhlhFtk0qeMD";
    private String consumerSecret = "EHP3zD4nX2oP8E0DrYpiPFB9ROVsD8Jcx845rp2KRbHmz6IsSI";
    private String accessToken = "1231360424936763393-4F8jwBLzff7wxw0sKzdBxVWPl3KZKV";
    private String accessTokenSecret = "eei8f0q2ifnX5kKSpxXXmWXR06KP0Gk1d7BvumUGbd317";
    
    SpoutOutputCollector twCollector;
    TwitterStream twStream;
    LinkedBlockingQueue twQueue = null;
    
    public TwitterSpout() {
         // Auto-generated constructor stub
    }

    @Override
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
       twQueue = new LinkedBlockingQueue();
       twCollector = collector;

       StatusListener listener = new StatusListener(){
         @Override
	 public void onException(Exception e) {
	       e.printStackTrace();
	 }

         @Override
	 public void onDeletionNotice(StatusDeletionNotice arg) {}

         @Override
	 public void onScrubGeo(long userId, long upToStatusId) {}

         @Override
	 public void onStallWarning(StallWarning warning) {}

         @Override
	 public void onStatus(Status status) {
	     twQueue.offer(status.getText().toLowerCase());
	 }

         @Override
	 public void onTrackLimitationNotice(int numberOfLimitedStatuses) {}
      };

      ConfigurationBuilder cb = new ConfigurationBuilder();
      cb.setDebugEnabled(true)
        .setOAuthConsumerKey(consumerKey)
	.setOAuthConsumerSecret(consumerSecret)
	.setOAuthAccessToken(accessToken)
	.setOAuthAccessTokenSecret(accessTokenSecret);

      twStream = new TwitterStreamFactory(cb.build()).getInstance();
      twStream.addListener(listener);
      twStream.sample("en");
    }

    @Override
    public void nextTuple() {
        Object ret = twQueue.poll();
        if (ret == null) {
	    Utils.sleep(1000);
	} else {
	   twCollector.emit(new Values(ret));
	}
    }

    @Override
    public void close() {
        twStream.shutdown();
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
       declarer.declare(new Fields("tweet"));
    }
}

